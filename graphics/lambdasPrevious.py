# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import os
from os.path import expanduser
from fnmatch import fnmatch
import numpy as np
from scipy import signal

if __name__ == '__main__':
    home = expanduser("~")
    patterns = ['weightsPrevious0.txt','weightsPrevious1.txt','weightsPrevious2.txt','weightsPrevious3.txt', 'weightsPrevious4.txt',
                'weightsPrevious5.txt','weightsPrevious6.txt','weightsPrevious7.txt','weightsPrevious8.txt','weightsPrevious9.txt',
                'weightsPrevious10.txt','weightsPrevious11.txt','weightsPrevious12.txt','weightsPrevious13.txt','weightsPrevious14.txt']

    colors = ['b','r','g','yellow','black','magenta','cyan','purple','orange','pink','grey','brown','#7777FF','#FF49BB','#18FAD2']

    weightsPrevious = []

    b, a = signal.butter(2, 0.001, analog=False)

    for pattern in patterns:
        for path, subdirs, files in os.walk(home + "/tcc/tests"):
            for name in files:
                if fnmatch(name, pattern):
                    with open(os.path.join(path, name), 'r', encoding='ISO-8859-1') as archive:
                        weights = []
                        for weight in archive:
                            weights.append(float(weight))

                        weights = signal.filtfilt(b, a, weights)

                        weightsPrevious.append(weights)

    plt.xlabel(u"Sentences")
    plt.ylabel(u"Value of lambda")

    index = 0
    for weights in weightsPrevious:
        plt.plot(range(1, len(weights)+1), weights, colors[index], label='Dist. ' + str(index+1))
        index += 1

    plt.legend(numpoints=1)
    plt.show()
