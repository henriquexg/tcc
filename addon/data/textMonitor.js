var element = document.activeElement;
var recomLength = 0;
var recom = false;
			console.log("monitor begin");


function setSelectionRange(input, selectionStart, selectionEnd) {
	if (input.setSelectionRange){
		input.focus();
		input.setSelectionRange(selectionStart, selectionEnd);
	}

	else if (input.createTextRange){
		var range = input.createTextRange();
		range.collapse(true);
		range.moveEnd('character', selectionEnd);
		range.moveStart('character', selectionStart);
		range.select();
	}
}

function setCaretToPos(input, pos) {
	setSelectionRange(input, pos, pos);
}

element.addEventListener('input', function() {
	if (recom || element.value.slice(0, element.value.length - recomLength).slice(-1) != ' '){
		element.value = element.value.slice(0, element.value.length);

		recom = false;
		recomLength = 0;
	}
	self.postMessage(element.value);
});

self.port.on('appendtext', function(text) {
	if (text != ''){
		element.value += text;
	}

	recomLength = text.length + 1;
	recom = true;

	setCaretToPos(element, element.value.length - text.length);
});

self.port.on('acceptrecom', function() {
	setCaretToPos(element, element.value.length);
	recom = false;
});

self.port.on('reqtext', function() {
	console.log(element.value);
	self.port.emit('sendtext', element.value);
});

self.port.on('noaccept', function(text) {
	var tam = element.value.length - text.length;
	element.value = element.value.slice(0,tam);
	setCaretToPos(element, element.value.length);
	recom = false;
});

self.port.on('block', function() {
	element.value.block;
});

self.port.on('release', function() {
	element.release;
});
