# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import os
from os.path import expanduser

if __name__ == '__main__':
    home = expanduser("~")
    path = "/home/henrique/tcc/tests/alpha.txt"

    alphas = []

    if os.path.exists(path):
        with open(path, "r") as archive:
            for line in archive:
                for word in line.split():
                    alphas.append(word)

    plt.xlabel(u"Sentences")
    plt.ylabel(u"Alpha")
    plt.plot(range(1, len(alphas)+1), alphas, 'b')
    plt.show()
