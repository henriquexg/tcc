# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import os
from os.path import expanduser
from scipy import signal

if __name__ == '__main__':
    home = expanduser("~")
    path1 = "/home/henrique/tcc/tests/alpha1.txt"
    path2 = "/home/henrique/tcc/tests/alpha2.txt"
    path3 = "/home/henrique/tcc/tests/alpha3.txt"
    path4 = "/home/henrique/tcc/tests/alpha4.txt"
    path5 = "/home/henrique/tcc/tests/alpha5.txt"

    array = []
    alphas1 = []
    alphas2 = []
    alphas3 = []
    alphas4 = []
    alphas5 = []

    if os.path.exists(path1):
        with open(path1, "r") as archive:
            for line in archive:
                for word in line.split():
                    array.append(word)

        for line in array:
            separator = line.find('@')

            alpha = line[separator + 1:]
            alphas1.append(float(alpha))

    array = []

    if os.path.exists(path2):
        with open(path2, "r") as archive:
            for line in archive:
                for word in line.split():
                    array.append(word)

        for line in array:
            separator = line.find('@')

            alpha = line[separator + 1:]
            alphas2.append(float(alpha))

    array = []

    if os.path.exists(path3):
        with open(path3, "r") as archive:
            for line in archive:
                for word in line.split():
                    array.append(word)

        for line in array:
            separator = line.find('@')

            alpha = line[separator + 1:]
            alphas3.append(float(alpha))

    array = []

    if os.path.exists(path4):
        with open(path4, "r") as archive:
            for line in archive:
                for word in line.split():
                    array.append(word)

        for line in array:
            separator = line.find('@')

            alpha = line[separator + 1:]
            alphas4.append(float(alpha))

    array = []

    if os.path.exists(path5):
        with open(path5, "r") as archive:
            for line in archive:
                for word in line.split():
                    array.append(word)

        for line in array:
            separator = line.find('@')

            alpha = line[separator + 1:]
            alphas5.append(float(alpha))

    b, a = signal.butter(2, 0.001, analog=False)

    alphas1 = signal.filtfilt(b, a, alphas1)
    alphas2 = signal.filtfilt(b, a, alphas2)
    alphas3 = signal.filtfilt(b, a, alphas3)
    alphas4 = signal.filtfilt(b, a, alphas4)
    alphas5 = signal.filtfilt(b, a, alphas5)

    plt.xlabel(u"Sentences")
    plt.ylabel(u"Alpha")

    p1 = plt.plot(range(len(alphas1)), alphas1, 'b', marker='>', markevery=20000, markersize=10, label='Config. 1')
    p2 = plt.plot(range(len(alphas2)), alphas2, 'r', marker='<', markevery=20000, markersize=10, label='Config. 2')
    p3 = plt.plot(range(len(alphas1)), alphas3, 'g', marker='8', markevery=20000, markersize=10, label='Config. 3')
    p4 = plt.plot(range(len(alphas1)), alphas4, 'yellow', marker='x', markevery=20000, markersize=10, label='Config. 4')
    p5 = plt.plot(range(len(alphas1)), alphas5, 'black', marker='*', markevery=20000, markersize=10, label='Config. 5')

    plt.legend(numpoints=1)

    plt.show()
