# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import os
from os.path import expanduser
from fnmatch import fnmatch
import numpy as np
from scipy import signal

if __name__ == '__main__':
    home = expanduser("~")

    weightsPrevious = []

    pattern0 = 'weightsPrevious0_1.txt'
    pattern1 = 'weightsPrevious0_2.txt'
    pattern2 = 'weightsPrevious0_3.txt'
    pattern3 = 'weightsPrevious0_4.txt'
    pattern4 = 'weightsPrevious0_5.txt'

    weights0 = []
    weights1 = []
    weights2 = []
    weights3 = []
    weights4 = []

    for path, subdirs, files in os.walk("/home/henrique/tcc/tests"):
        for name in files:
            if fnmatch(name, pattern0):
                with open(os.path.join(path, name), 'r', encoding='ISO-8859-1') as archive:
                    for weight in archive:
                        weights0.append(float(weight))

    for path, subdirs, files in os.walk("/home/henrique/tcc/tests"):
        for name in files:
            if fnmatch(name, pattern1):
                with open(os.path.join(path, name), 'r', encoding='ISO-8859-1') as archive:
                    for weight in archive:
                        weights1.append(float(weight))

    for path, subdirs, files in os.walk("/home/henrique/tcc/tests"):
        for name in files:
            if fnmatch(name, pattern2):
                with open(os.path.join(path, name), 'r', encoding='ISO-8859-1') as archive:
                    for weight in archive:
                        weights2.append(float(weight))

    for path, subdirs, files in os.walk("/home/henrique/tcc/tests"):
        for name in files:
            if fnmatch(name, pattern3):
                with open(os.path.join(path, name), 'r', encoding='ISO-8859-1') as archive:
                    for weight in archive:
                        weights3.append(float(weight))

    for path, subdirs, files in os.walk("/home/henrique/tcc/tests"):
        for name in files:
            if fnmatch(name, pattern4):
                with open(os.path.join(path, name), 'r', encoding='ISO-8859-1') as archive:
                    for weight in archive:
                        weights4.append(float(weight))

    b, a = signal.butter(2, 0.001, analog=False)

    weights0 = signal.filtfilt(b, a, weights0)
    weights1 = signal.filtfilt(b, a, weights1)
    weights2 = signal.filtfilt(b, a, weights2)
    weights3 = signal.filtfilt(b, a, weights3)
    weights4 = signal.filtfilt(b, a, weights4)

    plt.xlabel(u"Sentences")
    plt.ylabel(u"Value of lambda (dist 0)")

    p1 = plt.plot(range(len(weights0)), weights0, 'b', marker='>', markevery=20000, label='Config. 1')
    p2 = plt.plot(range(len(weights0)), weights1, 'r', marker='<', markevery=20000, label='Config. 2')
    p3 = plt.plot(range(len(weights0)), weights2, 'g', marker='8', markevery=20000, label='Config. 3')
    p4 = plt.plot(range(len(weights0)), weights3, 'yellow', marker='x', markevery=20000, label='Config. 4')
    p5 = plt.plot(range(len(weights0)), weights4, 'black', marker='*', markevery=20000, markersize=8, label='Config. 5')

    plt.legend(numpoints=1)

    plt.show()
