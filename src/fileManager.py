import re
import os
from os.path import expanduser
from fnmatch import fnmatch
import json
from collections import defaultdict
import nltk
import sys
import string
import pickle

class FileManager:
    def __init__(self):
        self.dic_to_index = {}

        index = 0

        self.word_to_index = {}
        self.index_to_word = {}
        self.graph_db = []
        self.aPriori = {}
        self.sum_col_db = defaultdict(dict)
        self.sum_row_db = defaultdict(dict)
        self.word_total = 0
        self.database = []
        self.map_lsa = {}

        self.graph_db_post = []
        self.sum_col_db_post = defaultdict(dict)

    def clean_str(self, text):
        text = re.sub(r"\[([^\]]+)\]", " ", text)
        text = re.sub(r"\(([^\)]+)\)", " ", text)
        text = re.sub(r"[^A-Za-z,!?.;]", " ", text)
        text = re.sub(r",", " , ", text)
        text = re.sub(r"!", " ! ", text)
        text = re.sub(r"\?", " ? ", text)
        text = re.sub(r";", " ; ", text)
        text = re.sub(r"\s{2,}", " ", text)

        return text.strip().lower()

    def fileOpenHolmes(self, root):
        pattern = '*.TXT'

        database_path = 'data/clean_database_holmes.pkl'

        if os.path.exists(database_path):
            print("Loading training database")
            with open(database_path, 'rb') as f:
                self.database = pickle.load(f)

        else:
            print('Opening data...')

            for path, subdirs, files in os.walk(root):
                for name in files:
                    if fnmatch(name, pattern):
                        sys.stdout.write(os.path.join(path, name) + '\r')

                        content = ''
                        with open(os.path.join(path, name), 'r', encoding='ISO-8859-1') as archive:
                            found = 0
                            for line in archive:
                                if line.find('*END*THE SMALL PRINT! FOR PUBLIC DOMAIN ETEXTS*') != -1 and not found:
                                    found = 1
                                    continue

                                if found:
                                    content += line + ' '

                            corpus = self.clean_str(content)
                            sentences = re.split(r"(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=[.?;!])\s", corpus)

                            for sent in sentences:
                                translator = str.maketrans('', '', string.punctuation)
                                s = sent.translate(translator)
                                tokens = s.strip().split()

                                self.word_total += len(tokens)
                                self.database.append(tokens)

                print('')

                word_total_json = {'word_total': self.word_total}
                with open('index/word_total.json', 'w') as fp:
                    json.dump(word_total_json, fp, sort_keys=True, indent=4)

                with open(database_path, 'wb') as f:
                    pickle.dump(self.database, f)

    def fileOpenMauro(self, root):
        pattern = '*.txt'

        print('Opening data...')

        for path, subdirs, files in os.walk(root):
            for name in files:
                if fnmatch(name, pattern):
                    sys.stdout.write(os.path.join(path, name) + '\r')

                    content = ''
                    with open(os.path.join(path, name), 'r', encoding='ISO-8859-1') as archive:
                        for line in archive:
                            content += line + ' '

                        corpus = self.clean_str(content)
                        sentences = re.split(r"(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=[.?;!])\s", corpus)

                        for sent in sentences:
                            translator = str.maketrans('', '', string.punctuation)
                            s = sent.translate(translator)
                            tokens = s.strip().split()

                            self.word_total += len(tokens)
                            self.database.append(tokens)

        print('')

        word_total_json = {'word_total': self.word_total}
        with open('index/word_total.json', 'w') as fp:
            json.dump(word_total_json, fp, sort_keys=True, indent=4)

    def fileOpenNSF(self, root):
        pattern = 'a*.txt'

        print('Opening data...')

        for path, subdirs, files in os.walk(root):
            for name in files:
                if fnmatch(name, pattern):
                    sys.stdout.write(os.path.join(path, name) + '\r')

                    content = ''
                    with open(os.path.join(path, name), 'r', encoding='ISO-8859-1') as archive:
                        found = 0
                        for line in archive:
                            if line.find('Abstract') != -1 and not found:
                                found = 1
                                continue

                            if found:
                                content += line + ' '

                        corpus = self.clean_str(content)
                        sentences = re.split(r"(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=[.?;!])\s", corpus)

                        for sent in sentences:
                            translator = str.maketrans('', '', string.punctuation)
                            s = sent.translate(translator)
                            tokens = s.strip().split()

                            self.word_total += len(tokens)
                            self.database.append(tokens)

        print('')

        word_total_json = {'word_total': self.word_total}
        with open('index/word_total.json', 'w') as fp:
            json.dump(word_total_json, fp, sort_keys=True, indent=4)

    def setGraph(self, number_graphs):
        print('Creating graphs...')

        for i in range(0, number_graphs):
            for indexWord in self.index_to_word:
                self.sum_col_db[i][indexWord] = 0

        for i in range(0, number_graphs):
            for indexWord in self.index_to_word:
                self.sum_row_db[i][indexWord] = 0

        for graphNum in range(0, number_graphs):
            sys.stdout.write('Graph - distance: ' + str(graphNum) + ' ...' + '\r')

            graph = defaultdict(dict)

            for content in self.database:
                itActual = 0
                itPrevious = itActual - graphNum - 1
                for i in range(0, len(content)):
                    if itPrevious > -1 and itActual < len(content):
                        self.sum_col_db[graphNum][self.word_to_index[content[itActual]]] += 1
                        self.sum_row_db[graphNum][self.word_to_index[content[itPrevious]]] += 1
                        try:
                            graph[self.word_to_index[content[itPrevious]]][self.word_to_index[content[itActual]]] += 1
                        except KeyError:
                            graph[self.word_to_index[content[itPrevious]]][self.word_to_index[content[itActual]]] = 1
                            pass
                    itPrevious += 1
                    itActual += 1

            with open('data/dict' + str(graphNum) + '.json', 'w') as fp:
                json.dump(graph, fp, sort_keys=True, indent=4)

            if graphNum >= (number_graphs - 1):
                break

            graphNum += 1

        print('')

        with open('data/sum_col_db.json', 'w') as fp:
            json.dump(dict(self.sum_col_db), fp, sort_keys=True, indent=4)

        with open('data/sum_row_db.json', 'w') as fp:
            json.dump(dict(self.sum_row_db), fp, sort_keys=True, indent=4)

    def setIndex(self):
        print('Creating index...')

        index = 0

        for content in self.database:
            for word in content:
                if word not in self.word_to_index:
                    self.word_to_index[word] = index
                    self.index_to_word[index] = word

                    index += 1
                try:
                    self.aPriori[self.word_to_index[word]] += 1
                except KeyError:
                    self.aPriori[self.word_to_index[word]] = 1

        self.word_to_index['@'] = index
        self.index_to_word[index] = '@'
        self.aPriori[self.word_to_index['@']] = 0

        with open('index/word_to_index.json', 'w') as fp:
            json.dump(self.word_to_index, fp, sort_keys=True, indent=4)
        with open('index/index_to_word.json', 'w') as fp:
            json.dump(self.index_to_word, fp, sort_keys=True, indent=4)
        with open('data/aPriori.json', 'w') as fp:
            json.dump(self.aPriori, fp, sort_keys=True, indent=4)

    def archive(self, number_graphs):
        print('Opening archive...')

        with open('index/word_total.json', 'r') as fp:
            word_total_js = json.load(fp)
            self.word_total = word_total_js['word_total']

        with open('index/word_to_index.json', 'r') as fp:
            self.word_to_index = json.load(fp)

        with open('index/index_to_word.json', 'r') as fp:
            self.index_to_word = json.load(fp)

            new_index_word = {}
            for index in range(0, len(self.index_to_word)):
                new_index_word[index] = self.index_to_word[str(index)]
            self.index_to_word = new_index_word
            del new_index_word

        with open('data/sum_col_db.json', 'r') as fp:
            self.sum_col_db = json.load(fp)

            output_dict = {}
            for key, value in self.sum_col_db.items():
                output_dict[int(key)] = {int(key2): value[key2] for key2 in value}

            self.sum_col_db = output_dict
            del output_dict

        with open('data/sum_row_db.json', 'r') as fp:
            self.sum_row_db = json.load(fp)

            output_dict = {}
            for key, value in self.sum_row_db.items():
                output_dict[int(key)] = {int(key2): value[key2] for key2 in value}

            self.sum_row_db = output_dict
            del output_dict

        with open('data/aPriori.json', 'r') as fp:
            self.aPriori = json.load(fp)

            new_aPriori = {}
            for index in range(0, len(self.aPriori)):
                new_aPriori[index] = self.aPriori[str(index)]
            self.aPriori = new_aPriori
            del new_aPriori

        for i in range(0, number_graphs):
            with open('data/dict' + str(i) + '.json', 'r') as fp:
                js = json.load(fp)

                output_dict = {}
                for key, value in js.items():
                    output_dict[int(key)] = {int(key2): value[key2] for key2 in value}

                self.graph_db.append(output_dict)
                del output_dict
