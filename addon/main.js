var cm = require('sdk/context-menu');
var data = require('sdk/self').data;
var pageMod = require("sdk/page-mod");
var self = require("sdk/self");
var tabs = require("sdk/tabs");

const {Cc, Ci} = require('chrome');
var xhr = Cc['@mozilla.org/xmlextras/xmlhttprequest;1'].createInstance(Ci.nsIXMLHttpRequest);

var { Hotkey } = require("sdk/hotkeys");

var url = 'http://gwachs.ddns.net:1433';
var count = 0;
var menu;
var monitor;
var text_;
var worker;
var accept = 0;
var nextword;

var id = 0;
var idrecep = 0;

var pageWorkers = require("sdk/page-worker");
pageWorkers.Page({
	include: "*",
	contentScriptFile: self.data.url('textMonitor.js'),
	contentScriptWhen: "ready",
	onMessage: function () {

		console.log("pagwrk");

	}
});

console.log("begin main");

function doRecommendation(res){
	if (res.hasOwnProperty('nextword')){
		nextword = res['nextword'];
		idrecep = res['id'];
		console.log(nextword);

		if (nextword.length > 0){
			var itemsJSON = new Array(nextword.length + 1);

			itemsJSON[0] = cm.Item({ label: 'Request recommendation...', data: '#recom' });

			for(var i = 0; i < nextword.length; i++){
				item = { label: nextword[i], data: nextword[i] };
				itemsJSON[i + 1] = cm.Item(item);
			}

			menu.items = itemsJSON;



		}
		else{
			menu.items = [
				cm.Item({ label: 'Request recommendation...', data: '#recom' }),
				cm.Item({ label: '<empty>', data: '#empty' })
			];
		}
	}
	else{
		menu.items = [
			cm.Item({ label: 'Request recommendation...', data: '#recom' }),
			cm.Item({ label: '<empty>', data: '#empty' })
		];
	}
}




monitor = pageMod.PageMod({
	include: "*",
	contentScriptFile: self.data.url('textMonitor.js'),
	attachTo: ['top', 'frame', 'existing'],
	onMessage: function (text) {
		if(text != text_){
			accept=0;
		}
		text_ = text;
		console.log('pageMod: ' + text);
		id = Math.random();
		if(text.slice(-1) === ' '){
			id = Math.random();
			req = {
				'previous':text_,
				'id':id
			}
			function reqListener() {
				res = JSON.parse(this.responseText);
				doRecommendation(res)
			}
			var urlRecom = url + '/inf';
			xhr.onload = reqListener;
			xhr.open('POST', urlRecom, false);
			xhr.setRequestHeader('Content-Type', 'application/json');
			xhr.send(JSON.stringify(req));

		}
	},
	onAttach: function(work) {
		worker = work;
		accept=0;
	}
});



menu = cm.Menu({
	label: 'Recommendations',
	context: cm.SelectorContext('input','textarea','pre'),
	contentScriptFile: data.url('recommend.js'),
	onMessage: function (text) {
		text_ = text;
		id = Math.random();
		req = {
			'previous':text,
			'id': id
		}


		function reqListener() {
			res = JSON.parse(this.responseText);
			doRecommendation(res);
		}

		var urlRecom = url + '/inf';
		xhr.onload = reqListener;
		xhr.open('POST', urlRecom, false);
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.send(JSON.stringify(req));
	},
	items: [
		cm.Item({ label: 'Request recommendation...', data: '#recom' }),
		cm.Item({ label: '<empty>', data: '#empty' })
	]
});


var nextwordHotkey = Hotkey({
	combo: "accel-alt-Space",
	onPress: function() {
		if(count < 10 && accept>1){
			count = count+1;
			worker.port.emit('noaccept', nextword[count - 1]);
			worker.port.emit('appendtext', nextword[count]);
			accept=2
			return;
		}
	}
});

var previouswordHotkey = Hotkey({
	combo: "accel-shift-Space",
	onPress: function() {
		if(count > 0 && accept>0){
			count = count-1;
			worker.port.emit('noaccept', nextword[count + 1]);
			worker.port.emit('appendtext', nextword[count]);
			accept=2
			return;
		}
	}
});

var noaccept = Hotkey({
		combo: "alt-shift-Space",
		onPress: function(){
			if(accept>0){
				accept=0;
				worker.port.emit('noaccept',nextword[count]);
				return;
			}
		}
});

var acceptHotkey = Hotkey({
	combo: "accel-Space",
	onPress: function() {
		if(accept===2){
			worker.port.emit('acceptrecom')
			accept=0;
			return;
		}

		if(accept===1){
			worker.port.emit('appendtext', nextword[0]);
			accept=2;
			console.log("accept");
			return;
		}

		if(accept===0 && idrecep===id){
			console.log("hotkey");
				worker.port.emit('reqtext');
				worker.port.on('sendtext', function(t){
					text = t;
					id = Math.random();
								req = {
									'previous':text_,
									'id':id
								}

								function reqListener() {
									res = JSON.parse(this.responseText);
									doRecommendation(res);
								}

								var urlRecom = url + '/inf';
								xhr.onload = reqListener;
								xhr.open('POST', urlRecom, false);
								xhr.setRequestHeader('Content-Type', 'application/json');
								xhr.send(JSON.stringify(req));
								accept=1;
								return;
				});
		}
	}
});
