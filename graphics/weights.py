# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import matplotlib
import os
from os.path import expanduser
from scipy import signal
import numpy as np

if __name__ == '__main__':
    lambdas_post = [
        2.7950037802256946,
        1.2125553300353906,
        0.8721441330403013,
        0.9357189159297528,
        0.8154316357362603,
        0.796971648518549,
        0.9732545267457118,
        1.1355012028682654,
        0.7627281205297392,
        0.952157910344988,
        1.090432710737216,
        0.9157327722200634,
        0.7895474149610459,
        0.7378737897073994,
        1.1916020378986094
    ]
    lambdas_previous = [
        5.216962007509733,
        1.0471925954600536,
        1.050079013032934,
        1.006258765768014,
        1.1457855225332305,
        0.8440801747987692,
        0.9064778491417906,
        0.7051828148244903,
        1.0245933124802182,
        1.1537495181357378,
        0.7480961543748291,
        0.6307524564073119,
        0.7345286720103398,
        0.6768313110534707,
        1.01043770693411
    ]

    # b, a = signal.butter(2, 0.5, analog=False)
    # lambdas_previous = signal.filtfilt(b, a, lambdas_previous)
    # lambdas_post = signal.filtfilt(b, a, lambdas_post)

    # plt.xlabel(u"Distance")
    plt.ylabel(u"Value of lambda")

    # p1 = plt.plot(range(0, len(lambdas_previous)), lambdas_previous, 'b', marker='8', markersize=8, label='Previous')
    # p2 = plt.plot(range(0, len(lambdas_previous)), lambdas_post, 'r', marker='x', markersize=8, label='Posterior')

    data1 = []
    data2 = []

    for i in range(0,len(lambdas_previous)):
        dt = (str(len(lambdas_previous)-1-i), lambdas_previous[len(lambdas_previous)-1-i])
        data1.append(dt)

    for i in range(0,len(lambdas_post)):
        dt = (str(-i), lambdas_post[i])
        data2.append(dt)

    N1 = len(data1)
    N2 = len(data2)

    x1 = np.arange(2, N1+2)
    x2 = np.arange(N1+2, N2*2+2)

    y1 = [num for (s, num) in data1]
    y2 = [num for (s, num) in data2]

    labels1 = [s for (s, num) in data1]
    labels2 = [s for (s, num) in data2]

    width = 1

    bar1 = plt.bar(x1, y1, width, color="b", edgecolor='black', hatch="//", label='Previous')
    bar2 = plt.bar(x2, y2, width, color="r", edgecolor='black', hatch="\\\\", label='Posterior')

    x = np.arange(1, N2*2+1)
    data = data1 + data2
    labels = [s for (s, num) in data]

    plt.xticks(x + width, labels)

    matplotlib.rcParams['legend.handleheight'] = 2
    plt.legend(numpoints=1)

    plt.show()
