import math
import numpy as np


class NaiveBayes:
    def __init__(self, aPriori, word_total, number_graphs, word_to_index, index_to_word, graph):
        self.index_to_word = index_to_word
        self.aPriori = aPriori
        self.word_total = word_total
        self.word_to_index = word_to_index
        self.graph = graph
        self.number_graphs = number_graphs

        self.weightsPrevious = []
        for i in range(0, number_graphs):
            self.weightsPrevious.append(1.0)

        self.weightsPost = []
        for i in range(0, number_graphs):
            self.weightsPost.append(1.0)

    def doHypothesis(self, previousIndex):
        evaluations = []
        gama = 0.0

        for pRecommendation in self.index_to_word:
            i = 0

            evaluation = float(self.aPriori.get(pRecommendation)) / float(self.word_total)

            for wordIndex in previousIndex:
                if wordIndex == self.word_to_index['@']:
                    evaluation *= pow(self.graph.getMin(), self.weightsPrevious[len(previousIndex)-1-i])
                    i += 1
                    continue

                value = self.graph.getValuePrevious(len(previousIndex)-1-i, wordIndex, pRecommendation)

                evaluation *= pow(value, self.weightsPrevious[len(previousIndex)-1-i])

                i += 1

            gama += evaluation
            evaluations.append({'evaluation': evaluation, 'index': pRecommendation})

        recommendations = [0] * len(self.index_to_word)
        if gama != 0.0:
            for i in range(0, len(evaluations)):
                recommendations[evaluations[i].get('index')] = evaluations[i].get('evaluation') / gama
        else:
            for i in range(0, len(evaluations)):
                recommendations[evaluations[i].get('index')] = evaluations[i].get('evaluation')

        return recommendations

    def doHypothesisTest(self, previousIndex, optionsIndex):
        evaluations = []
        gama = 0.0

        for pRecommendation in optionsIndex:
            if pRecommendation == self.word_to_index['@']:
                evaluation = self.graph.getMin()
                gama += evaluation
                evaluations.append({'evaluation': evaluation, 'index': pRecommendation})
            else:
                i = 0
                evaluation = float(self.aPriori.get(pRecommendation)) / float(self.word_total)

                for wordIndex in previousIndex:
                    if wordIndex == self.word_to_index['@']:
                        evaluation *= pow(self.graph.getMin(), self.weightsPrevious[len(previousIndex)-1-i])
                        i += 1
                        continue

                    value = self.graph.getValuePrevious(len(previousIndex)-1-i, wordIndex, pRecommendation)

                    evaluation *= pow(value, self.weightsPrevious[len(previousIndex)-1-i])
                    # evaluation *= value

                    i += 1

                gama += evaluation
                evaluations.append({'evaluation': evaluation, 'index': pRecommendation})

        recommendations = []
        if gama != 0.0:
            for i in range(0, len(evaluations)):
                recommendations.append(evaluations[i].get('evaluation') / gama)
        else:
            for i in range(0, len(evaluations)):
                recommendations.append(evaluations[i].get('evaluation'))

        return recommendations

    def doHypothesisTestFull(self, previousIndex, postIndex, optionsIndex):
        evaluations = []
        gama = 0.0

        for pRecommendation in optionsIndex:
            if pRecommendation == self.word_to_index['@']:
                evaluation = self.graph.getMin()
                gama += evaluation
                evaluations.append({'evaluation': evaluation, 'index': pRecommendation})
            else:
                i = 0
                evaluation = float(self.aPriori.get(pRecommendation)) / float(self.word_total)

                for wordIndex in previousIndex:
                    if wordIndex == self.word_to_index['@']:
                        evaluation *= pow(self.graph.getMin(), self.weightsPrevious[len(previousIndex)-1-i])
                        i += 1
                        continue

                    value = self.graph.getValuePrevious(len(previousIndex)-1-i, wordIndex, pRecommendation)

                    evaluation *= pow(value, self.weightsPrevious[len(previousIndex)-1-i])
                    # evaluation *= value

                    i += 1

                i = 0

                for wordIndex in postIndex:
                    if wordIndex == self.word_to_index['@']:
                        evaluation *= pow(self.graph.getMin(), self.weightsPost[i])
                        i += 1
                        continue

                    value = self.graph.getValuePost(i, pRecommendation, wordIndex)

                    evaluation *= pow(value, self.weightsPost[i])
                    # evaluation *= value

                    i += 1

                gama += evaluation
                evaluations.append({'evaluation': evaluation, 'index': pRecommendation})

        recommendations = []
        if gama != 0.0:
            for i in range(0, len(evaluations)):
                recommendations.append(evaluations[i].get('evaluation') / gama)
        else:
            for i in range(0, len(evaluations)):
                recommendations.append(evaluations[i].get('evaluation'))

        return recommendations

    def updateLambdaPrevious(self, previousIndex, optionsIndex, alpha, teta, tetaBayes, etaLambda, correctIndex):
        pRecommendation = optionsIndex[correctIndex]

        if not pRecommendation == self.word_to_index['@'] and len(previousIndex) > 1:
            t = []
            for i in range(0, len(previousIndex)):
                t.append(0.0)

            i = 0

            for wordIndex in previousIndex:
                if wordIndex == self.word_to_index['@']:
                    t[len(previousIndex)-1-i] = self.graph.getMin()
                    i += 1
                    continue

                value = self.graph.getValuePrevious(len(previousIndex)-1-i, wordIndex, pRecommendation)
                t[len(previousIndex)-1-i] = value

                i += 1

            lambdas_previous = self.weightsPrevious.copy()
            weights = 1.0

            for key in range(1, len(previousIndex)):
                weightsGrad = 1.0
                for w in range(1, len(previousIndex)):
                    if w == key:
                        weightsGrad *= pow(self.weightsPrevious[w], 2.0)
                    else:
                        weightsGrad *= self.weightsPrevious[w]

                update_in = alpha * tetaBayes * (math.log(t[key]) - (math.log(t[0]) / weightsGrad))
                weight = self.weightsPrevious[key] + (1.0 - teta) * update_in * etaLambda
                lambdas_previous[key] = weight
                weights *= weight

            self.weightsPrevious = lambdas_previous

            self.weightsPrevious[0] = 1.0 / weights

    def updateLambdaPost(self, postIndex, optionsIndex, alpha, teta, tetaBayes, etaLambda, correctIndex):
        pRecommendation = optionsIndex[correctIndex]

        if not pRecommendation == self.word_to_index['@'] and len(postIndex) > 1:
            t = []
            i = 0

            for wordIndex in postIndex:
                if wordIndex == self.word_to_index['@']:
                    t.append(self.graph.getMin())
                    i += 1
                    continue

                value = self.graph.getValuePost(i, pRecommendation, wordIndex)
                t.append(value)

                i += 1

            lambdas_post = self.weightsPost.copy()
            weights = 1.0

            for key in range(1, len(postIndex)):
                weightsGrad = 1.0
                for w in range(1, len(postIndex)):
                    if w == key:
                        weightsGrad *= pow(self.weightsPost[w], 2.0)
                    else:
                        weightsGrad *= self.weightsPost[w]

                update_in = alpha * tetaBayes * (math.log(t[key]) - (math.log(t[0]) / weightsGrad))
                weight = self.weightsPost[key] + (1.0 - teta) * update_in * etaLambda
                lambdas_post[key] = weight
                weights *= weight

            self.weightsPost = lambdas_post

            self.weightsPost[0] = 1.0 / weights
