# -*- coding: utf-8 -*-
import os
from os.path import expanduser
from fnmatch import fnmatch

if __name__ == '__main__':
    home = expanduser('~')
    directory = home + '/tcc'
    pattern = '*.so'

    for path, subdirs, files in os.walk(directory):
        for name in files:
            if fnmatch(name, pattern):
                pos = name.find('.')
                new_name = name[:pos]
                new_name += '.so'
                os.rename(os.path.join(path, name), new_name)
