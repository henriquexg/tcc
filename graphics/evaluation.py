# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import os
from os.path import expanduser

if __name__ == '__main__':
    home = expanduser("~")
    path = "/home/henrique/tcc/tests/evaluation.txt"

    array = []
    alphas = []
    evaluations = []

    if os.path.exists(path):
        with open(path, "r") as archive:
            for line in archive:
                for word in line.split():
                    array.append(word)

        for line in array:
            separator = line.find('@')

            alpha = line[:separator]
            alphas.append(alpha)

            evaluation = line[separator + 1:]
            evaluations.append(evaluation)

    plt.xlabel(u"Alpha")
    plt.ylabel(u"Evaluation")
    plt.plot(alphas, evaluations, 'b')
    plt.show()
