# -*- coding: utf-8 -*-
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import os
from os.path import expanduser
import plotly.plotly as py

if __name__ == '__main__':
    home = expanduser("~")
    pathOpt = "/home/henrique/tcc/tests/evaluationFull.txt"
    pathOpt_n = "/home/henrique/tcc/tests/evaluationFull_n.txt"

    evaluations = []

    if os.path.exists(pathOpt):
        with open(pathOpt, "r") as archive:
            for line in archive:
                evaluations.append(line)

        raw_data = {
            'x1': [evaluations[0]],
            'x2': [evaluations[1]],
            'x3': [evaluations[2]],
            'x4': [evaluations[3]],
            'x5': [evaluations[4]]
            }
        df = pd.DataFrame(raw_data, columns = ['x1', 'x2', 'x3', 'x4', 'x5'])

    evaluations_n = []

    if os.path.exists(pathOpt_n):
        with open(pathOpt_n, "r") as archive:
            for line in archive:
                evaluations_n.append(line)

        raw_data = {
            'x1': [evaluations_n[0]],
            'x2': [evaluations_n[1]],
            'x3': [evaluations_n[2]],
            'x4': [evaluations_n[3]],
            'x5': [evaluations_n[4]]
            }
        df_n = pd.DataFrame(raw_data, columns = ['x1', 'x2', 'x3', 'x4', 'x5'])

    N = 5
    ind = np.arange(N)  # the x locations for the groups
    width = 0.35       # the width of the bars

    fig, ax = plt.subplots()

    opt_means = [df['x1'], df['x2'], df['x3'], df['x4'], df['x5']]
    rects1 = ax.bar(ind, opt_means, width, color='b', edgecolor='black', hatch="//")

    n_opt_means = [df_n['x1'], df_n['x2'], df_n['x3'], df_n['x4'], df_n['x5']]
    rects2 = ax.bar(ind + width, n_opt_means, width, color='r', edgecolor='black', hatch="O")

    ###############################################################################################

    ind_comp = np.arange(N+3)

    rects3 = ax.bar(ind_comp[-3:]+width/2, [0.589, 0.49, 0.41], width, color='#00FF55', edgecolor='black', hatch="\\\\")

    ###############################################################################################

    # add some text for labels, title and axes ticks
    ax.set_ylabel('Evaluation')
    ax.set_xticks(ind_comp + width / 2)
    ax.set_xticklabels(('Conf. 1', 'Conf. 2', 'Conf. 3', 'Conf. 4', 'Conf. 5', 'A', 'B', 'C'))

    matplotlib.rcParams['legend.handleheight'] = 2

    ax.legend((rects1[0], rects2[0], rects3[0]), ('Optimized', 'Not optimized', 'Another model'), prop={'size':9}, loc=2)

    axes = plt.gca()
    axes.set_ylim([0.0,0.8])

    plt.show()
