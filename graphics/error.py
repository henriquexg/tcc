# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import os
from os.path import expanduser

if __name__ == '__main__':
    home = expanduser("~")
    path = "/home/henrique/tcc/tests/error.txt"

    errors = []

    if os.path.exists(path):
        with open(path, "r") as archive:
            for line in archive:
                errors.append(line)

    plt.xlabel(u"Epoch")
    plt.ylabel(u"Error")
    plt.plot(range(1, len(errors)+1), errors, 'b')
    plt.show()
