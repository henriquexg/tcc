self.on('click', function (node, data) {
	var element = document.activeElement;

	if (data != '#empty' && data != '#recom'){
		console.log('You clicked - ' + data);

		if(element.type === 'text'){
			element.value += data;
		}
	}
	else if(data == '#recom'){
		console.log('Recommendation');

		if(element.type === 'text'){
			self.postMessage(element.value);
		}
	}
});
