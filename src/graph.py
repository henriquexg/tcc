class Graph:
    def __init__(self, word_total, sum_col_db, sum_row_db, graph_db):
        self.word_total = word_total
        self.sum_col_db = sum_col_db
        self.sum_row_db = sum_row_db
        self.graph_db = graph_db

    def getValuePrevious(self, graphIndex, src, dst):
        presearch = self.graph_db[graphIndex].get(src)
        if presearch:
            search = presearch.get(dst)
            if search:
                normalizer = self.sum_col_db.get(graphIndex).get(dst)
                return float(search) / float(normalizer)
            else:
                return self.getMin()
        else:
            return self.getMin()

    def getValuePost(self, graphIndex, src, dst):
        presearch = self.graph_db[graphIndex].get(src)
        if presearch:
            search = presearch.get(dst)
            if search:
                normalizer = self.sum_row_db.get(graphIndex).get(dst)
                return float(search) / float(normalizer)
            else:
                return self.getMin()
        else:
            return self.getMin()

    def getMin(self):
        return 1.0 / float(self.word_total)

    def getVectorPrevious(self, graphIndex, wordIndex):
        vec = []
        search = self.graph_db[graphIndex].get(wordIndex)
        if search:
            for word in search:
                vec.append(word)

        return vec
