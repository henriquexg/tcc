# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import os
from os.path import expanduser
from scipy import signal

if __name__ == '__main__':
    home = expanduser("~")
    path1 = "/home/henrique/tcc/tests/errorNaive.txt"
    path2 = "/home/henrique/tcc/tests/errorLSA.txt"
    path3 = "/home/henrique/tcc/tests/errorBoth.txt"

    errorsNaive = []

    if os.path.exists(path1):
        with open(path1, "r") as archive:
            for line in archive:
                errorsNaive.append(float(line))

    errorsLSA = []

    if os.path.exists(path2):
        with open(path2, "r") as archive:
            for line in archive:
                errorsLSA.append(float(line))

    errorsFull = []

    if os.path.exists(path3):
        with open(path3, "r") as archive:
            for line in archive:
                errorsFull.append(float(line))

    # b, a = signal.butter(2, 0.2, analog=False)
    # errorsNaive = signal.filtfilt(b, a, errorsNaive)
    # errorsLSA = signal.filtfilt(b, a, errorsLSA)
    # errorsFull = signal.filtfilt(b, a, errorsFull)

    plt.xlabel(u"Size of history")
    plt.ylabel(u"Error")

    p1 = plt.plot(range(2,16), errorsNaive, 'b', marker='>', label='Only Naive Bayes')
    p2 = plt.plot(range(2,16), errorsLSA, 'r', marker='<', label='Only LSA')
    p3 = plt.plot(range(2,16), errorsFull, 'g', marker='8', label='Both')

    plt.legend(numpoints=1)
    plt.show()
