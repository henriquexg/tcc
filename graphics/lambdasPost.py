# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import os
from os.path import expanduser
from fnmatch import fnmatch
import numpy as np
from scipy import signal

if __name__ == '__main__':
    home = expanduser("~")
    patterns = ['weightsPost0.txt','weightsPost1.txt','weightsPost2.txt','weightsPost3.txt', 'weightsPost4.txt',
                'weightsPost5.txt','weightsPost6.txt','weightsPost7.txt','weightsPost8.txt','weightsPost9.txt',
                'weightsPost10.txt','weightsPost11.txt','weightsPost12.txt','weightsPost13.txt','weightsPost14.txt']

    colors = ['b','r','g','yellow','black','magenta','cyan','purple','orange','pink','grey','brown','#7777FF','#FF49BB','#18FAD2']

    weightsPost = []

    b, a = signal.butter(2, 0.001, analog=False)

    for pattern in patterns:
        for path, subdirs, files in os.walk(home + "/tcc/tests"):
            for name in files:
                if fnmatch(name, pattern):
                    with open(os.path.join(path, name), 'r', encoding='ISO-8859-1') as archive:
                        weights = []
                        for weight in archive:
                            weights.append(float(weight))

                        weights = signal.filtfilt(b, a, weights)

                        weightsPost.append(weights)

    plt.xlabel(u"Sentences")
    plt.ylabel(u"Value of lambda")
    index = 0

    for weights in weightsPost:
        plt.plot(range(1, len(weights)+1), weights, colors[index], label='Dist. ' + str(index))
        index += 1

    plt.legend(numpoints=1)
    plt.show()
