import argparse
import os
import string
import nltk
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import TruncatedSVD
from sklearn.preprocessing import Normalizer
import numpy as np
import time
import csv
import random
import glob
import pickle
import re
from scipy import spatial
import json
import sys


class LSA:
    def __init__(self, word_to_index, index_to_word, data, path_test_file, path_output, path_training, graph):
        self.word_to_index = word_to_index
        self.index_to_word = index_to_word
        self.map = {}
        self.data = data
        self.path_test_file = path_test_file
        self.path_output = path_output
        self.path_training = path_training
        self.graph = graph

        if not os.path.exists(self.path_output):
            os.makedirs(self.path_output)

        corpus = []
        if os.path.exists(os.path.join(path_output, 'clean_data.pkl')):
            corpus = self.load_train('', self.path_training)
        else:
            for path in self.data:
                corpus += self.load_train(path, self.path_training)

        print("Total training sentences: {}".format(len(corpus)))

        self.word_dict = self.build_lsa_feature(corpus, self.path_training)

    def clean_str(self, text):
        text = re.sub(r"\[([^\]]+)\]", " ", text)
        text = re.sub(r"\(([^\)]+)\)", " ", text)
        text = re.sub(r"[^A-Za-z,!?.;]", " ", text)
        text = re.sub(r",", " , ", text)
        text = re.sub(r"!", " ! ", text)
        text = re.sub(r"\?", " ? ", text)
        text = re.sub(r";", " ; ", text)
        text = re.sub(r"\s{2,}", " ", text)

        return text.strip().lower()

    def preprocess(self, input_files, data_path):
        all_corpus = []
        for input_file in input_files:
            with open(input_file, "r", encoding='utf-8', errors='ignore') as f:
                sys.stdout.write(
                    str(input_files.index(input_file)) + ' of ' + str(len(input_files)) + ' | ' + input_file + '\r')

                corpus = ''

                found = 0
                for line in f:
                    if line.find('*END*THE SMALL PRINT! FOR PUBLIC DOMAIN ETEXTS*') != -1 and not found:
                        found = 1
                        continue

                    if found:
                        corpus += line + ' '

                corpus = self.clean_str(corpus)
                sentences = re.split(r"(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=[.?;!])\s", corpus)

                clean_sent = []
                for sent in sentences:
                    translator = str.maketrans('', '', string.punctuation)
                    s = sent.translate(translator)
                    tokens = s.strip().split()
                    important_words = []
                    for word in tokens:
                        if word not in stopwords.words('english'):
                            important_words.append(word)
                    if len(important_words) > 5:
                        clean_sent.append(" ".join(important_words))
            all_corpus += clean_sent

        print('')

        with open(data_path, 'wb') as f:
            pickle.dump(all_corpus, f)

        print("Saved clean corpus at {}".format(data_path))

        return all_corpus

    def load_train(self, data_dir, save_dir):
        input_files = glob.glob(os.path.join(data_dir, "*.TXT"))

        data_file = os.path.join(save_dir, 'clean_data.pkl')

        if os.path.exists(data_file):
            print("Loading preprocessed training data")
            with open(data_file, 'rb') as f:
                clean_corpus = pickle.load(f)
        else:
            print("Processing training data")
            clean_corpus = self.preprocess(input_files, data_file)

        return clean_corpus

    def build_lsa_feature(self, corpus, save_dir):
        dict_file = os.path.join(save_dir, 'lsa_word_dict.pkl')
        if os.path.exists(dict_file):
            print("Loading LSA word dictionary")
            with open(dict_file, 'rb') as f:
                word_dict = pickle.load(f)
        else:
            print("Building word features with LSA")

            vectorizer = CountVectorizer(analyzer='word', tokenizer=None, preprocessor=None, stop_words=None)
            bow_features = vectorizer.fit_transform(corpus)

            lsa = TruncatedSVD(500, algorithm='arpack')
            word_features = lsa.fit_transform(bow_features.T)
            word_features = Normalizer(copy=False).fit_transform(word_features)

            vocabulary = vectorizer.get_feature_names()
            word_dict = {}
            for i, word in enumerate(vocabulary):
                word_dict[word] = word_features[i]

            with open(dict_file, 'wb') as f:
                pickle.dump(word_dict, f)

        return word_dict

    def word2featureQues(self, tokens, embeddings):
        word_vec = []

        for word in tokens:
            if word in embeddings:
                word_vec.append(embeddings[word])

        return word_vec

    def word2featureCand(self, tokens, embeddings):
        word_vec = []

        for word in tokens:
            if word in embeddings:
                word_vec.append(embeddings[word])
            else:
                word_vec.append([])

        return word_vec

    def total_similarity(self, word, ques_vec):
        score = 0.0

        for v in ques_vec:
            score += 1.0 / (spatial.distance.euclidean(word, v) + 1.0)

        score /= float(len(ques_vec))

        return score

    def testLSA(self):
        start = time.time()

        keys = ['a)', 'b)', 'c)', 'd)', 'e)']
        choices = ['a', 'b', 'c', 'd', 'e']
        prediction = []

        print("Predicting answers")
        with open(self.path_test_file, 'r', encoding='utf-8') as f:
            reader = csv.DictReader(f)
            for i, row in enumerate(reader):
                question = row['question']
                ulPos = question.index('_')
                question = question[:ulPos]
                translator = str.maketrans('', '', string.punctuation)
                question = question.translate(translator)
                tokens = question.split()

                ques_vec = self.word2feature(tokens, self.word_dict)

                scores = []
                candidates = [row[x] for x in keys]
                cand_vec = self.word2feature(candidates, self.word_dict)
                for word in cand_vec:
                    score = self.total_similarity(word, ques_vec)
                    scores.append(score)

                idx = scores.index(max(scores))
                ans = choices[idx]
                prediction.append(ans)

        output = os.path.join(self.path_output, "prediction.csv")
        with open(output, 'w') as out:
            writer = csv.writer(out, delimiter=',')
            writer.writerow(['id', 'answer'])
            for i, ans in enumerate(prediction):
                writer.writerow([str(i + 1), ans])
        print("Output prediction file: {}".format(output))

        print("Done in {:3f}s".format(time.time() - start))

    def doHypothesis(self, tokens, candidates):
        recommendations = []

        ques_vec = self.word2featureQues(tokens, self.word_dict)
        cand_vec = self.word2featureCand(candidates, self.word_dict)

        for word in cand_vec:
            if len(word) == 0 or len(ques_vec) == 0:
                recommendations.append({'score': 0.0, 'word': candidates[len(recommendations)-1]})
                continue
            scoreLSA = self.total_similarity(word, ques_vec)
            recommendations.append({'score': scoreLSA, 'word': candidates[len(recommendations)-1]})

        return recommendations

    def doHypothesisTest(self, tokens, candidates):
        recommendations = []

        ques_vec = self.word2featureQues(tokens, self.word_dict)
        cand_vec = self.word2featureCand(candidates, self.word_dict)

        for word in cand_vec:
            if len(word) == 0 or len(ques_vec) == 0:
                recommendations.append(0.0)
                continue
            scoreLSA = self.total_similarity(word, ques_vec)
            recommendations.append(scoreLSA)

        return recommendations
