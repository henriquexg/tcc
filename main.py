from os.path import expanduser
import sys
from lib.fileManager import FileManager
from lib.naive import NaiveBayes
from lib.lsa import LSA
from lib.graph import Graph
import json
from collections import defaultdict
from flask import Flask, request
import nltk
from nltk.corpus import stopwords
import re
import os
import string
import csv
import random
import time
import numpy as np
import threading
from fnmatch import fnmatch
import glob
import pickle

global naive
global lsa
global fileManager
global number_graphs
global config
global alpha
global r

app = Flask(__name__)

@app.route('/inf', methods=['POST'])
def jsonInfPost():
    if request.json:
        mydata = request.json

        previous = mydata.get('previous')
        previous = clean_str(previous)
        sentences = re.split(r"(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=[.?;!])\s", previous)

        if len(sentences) > 0:
            previous = sentences[-1]
        else:
            previous = []

        tokens = previous.split()
        tokens = tokens[-number_graphs:]
        print(tokens)

        ########################################################################## Naive

        previousIndex = conv_str_index(tokens)

        if len(previousIndex) < number_graphs:
            vec = []
            for index in range(0, number_graphs - len(previousIndex)):
                vec.append(fileManager.word_to_index.get('@'))
            for wordIndex in previousIndex:
                vec.append(wordIndex)
            previousIndex = vec

        recommendationsNaive = naive.doHypothesis(previousIndex)

        ########################################################################## LSA

        adj_words = []
        inter = []
        for wordIndex in previousIndex:
            adj_words.append(np.array(graph.getVectorPrevious(len(previousIndex) - 1 - len(adj_words), wordIndex)))

        if len(adj_words) > 1:
            inter = np.intersect1d(adj_words[0], adj_words[1])
            for v_index in range(2, len(adj_words)):
                inter = np.intersect1d(adj_words[v_index], inter)
        elif len(adj_words) == 1:
            inter = adj_words[0]

        candidates = []
        for wordIndex in inter:
            if fileManager.index_to_word.get(wordIndex) not in stopwords.words('english'):  
                candidates.append(fileManager.index_to_word.get(wordIndex))

        recommendationsLSA = lsa.doHypothesis(tokens, candidates)

        recommendationsLSA_norm = []
        for word in fileManager.word_to_index:
            recommendationsLSA_norm.append(0.0)

        for score in recommendationsLSA:
            wordIndex = fileManager.word_to_index.get(score.get('word'))
            if wordIndex:
                recommendationsLSA_norm[wordIndex] = score.get('score')

        recommendationsLSA = recommendationsLSA_norm

        ########################################################################## Equalization

        recommendationsLSA = equalization(recommendationsLSA)
        recommendationsNaive = equalization(recommendationsNaive)

        ######################################################################### Inference

        recommendations = []
        for i in range(0, len(recommendationsLSA)):
            recommendation = alpha * recommendationsNaive[i] + (1.0 - alpha) * recommendationsLSA[i]
            recommendations.append(recommendation)

        recommendationsRes = []

        for i in range(0, r):
            idx = recommendations.index(max(recommendations))
            word = fileManager.index_to_word.get(idx)

            if word == '@':
                recommendations.pop(idx)
                idx = recommendations.index(max(recommendations))
                word = fileManager.index_to_word.get(idx)

            recommendationsRes.append(word)
            recommendations.pop(idx)

        data = {}
        data['nextword'] = recommendationsRes
        data['id'] = mydata.get('id')

        res = json.dumps(data)

        return app.response_class(res, content_type='application/json')

    else:
        return 'no json received'

@app.route('/test', methods=['POST'])
def jsonTestPost():
    if request.json:
        eta = config['eta']
        etaLambda = config['etaLambda']
        times = config['times']
        param_update = config['param_update']

        mydata = request.json

        if mydata.get('alpha'):
            alpha = float(mydata.get('alpha'))
        if mydata.get('eta'):
            eta = float(mydata.get('eta'))
        if mydata.get('etaLambda'):
            etaLambda = float(mydata.get('etaLambda'))
        if mydata.get('times'):
            times = int(mydata.get('times'))
        if mydata.get('param_update'):
            param_update = int(mydata.get('param_update'))
        if mydata.get('lambdas_previous'):
            naive.weightsPrevious = mydata.get('lambdas_previous')

        answersDict = csv.DictReader(open(config['path_test'] + '/test_answer.csv'))
        answers = []
        for row in answersDict:
            answers.append(row['answer'])

        scores = []
        keys = ['a)', 'b)', 'c)', 'd)', 'e)']
        choices = ['a', 'b', 'c', 'd', 'e']

        if os.path.exists(config['path_test'] + '/evaluation.txt'):
            os.remove(config['path_test'] + '/evaluation.txt')

        test_count = 1

        for t in range(0, times):
            start = time.time()

            prediction = []

            if param_update:
                print("Optimizing")

                naive.weightsPrevious = []
                for ng in range(0, number_graphs):
                    naive.weightsPrevious.append(1.0)

                with open(lsa.path_test_file, 'r', encoding='utf-8') as f:
                    reader = csv.DictReader(f)
                    for i, row in enumerate(reader):
                        question = row['question']
                        ulPos = question.index('_')
                        question = question[:ulPos]

                        tokens = clean_punct(question).split()
                        tokens = tokens[-number_graphs:]

                        candidates = [row[x] for x in keys]

                        ########################################################################## Naive
                        questionIndex = conv_str_index(tokens)

                        if len(questionIndex) < number_graphs:
                            vec = []
                            for index in range(0, number_graphs - len(questionIndex)):
                                vec.append(fileManager.word_to_index.get('@'))
                            for wordIndex in questionIndex:
                                vec.append(wordIndex)
                            questionIndex = vec

                        optionsIndex = []
                        for word in candidates:
                            if word in fileManager.word_to_index and word != '':
                                optionsIndex.append(fileManager.word_to_index.get(word))
                            else:
                                optionsIndex.append(fileManager.word_to_index.get('@'))

                        recommendationsNaive = naive.doHypothesisTest(questionIndex, optionsIndex)

                        ########################################################################## LSA

                        recommendationsLSA = lsa.doHypothesisTest(tokens, candidates)

                        ######################################################################### Equalization

                        recommendationsLSA = equalization(recommendationsLSA)
                        recommendationsNaive = equalization(recommendationsNaive)

                        ######################################################################### Teta + correct
                        recommendations = []
                        for recom in range(0, len(recommendationsLSA)):
                            recommendation = alpha * recommendationsNaive[recom] + (1.0 - alpha) * recommendationsLSA[recom]
                            recommendations.append(recommendation)

                        idx = recommendations.index(max(recommendations))
                        ans = choices[idx]
                        prediction.append(ans)

                        correctIndex = choices.index(answers[len(prediction) - 1])
                        teta = recommendations[correctIndex]

                        ######################################################################### Lambda

                        tetaBayes = recommendationsNaive[correctIndex]

                        naive.updateLambdaPrevious(questionIndex, optionsIndex, alpha, teta, tetaBayes, etaLambda, correctIndex)

                        ######################################################################### Alpha

                        alpha = alpha + (1.0 - teta) * (recommendationsNaive[correctIndex] - recommendationsLSA[correctIndex]) * eta
                        if alpha < 0.0:
                            alpha = 0.0
                        elif alpha > 1.0:
                            alpha = 1.0

                        score = 0
                        if answers[len(prediction) - 1] == prediction[len(prediction) - 1]:
                            score = 1
                        score = float(score)
                        scores.append({'score': score, 'alpha': alpha})

                    # print ('Alpha: ' + str(alpha))
                    # print ('Lambdas: ' + str(naive.weightsPrevious))

            with open(config['path_test'] + '/alpha.txt', 'w') as archive:
                index = 1
                for test_score in scores:
                    l = str(index) + '@' + str(test_score.get('alpha'))
                    l = l + "\n"
                    index += 1
                    archive.write(l)

            prediction = []

            print("Testing")
            with open(lsa.path_test_file, 'r', encoding='utf-8') as f:
                reader = csv.DictReader(f)
                for i, row in enumerate(reader):
                    question = row['question']
                    ulPos = question.index('_')
                    question = question[:ulPos]

                    tokens = clean_punct(question).split()
                    tokens = tokens[-number_graphs:]

                    candidates = [row[x] for x in keys]

                    ########################################################################## Naive
                    questionIndex = conv_str_index(tokens)

                    optionsIndex = []
                    for word in candidates:
                        if word in fileManager.word_to_index and word != '':
                            optionsIndex.append(fileManager.word_to_index.get(word))
                        else:
                            optionsIndex.append(fileManager.word_to_index.get('@'))

                    recommendationsNaive = naive.doHypothesisTest(questionIndex, optionsIndex)

                    ########################################################################## LSA

                    recommendationsLSA = lsa.doHypothesisTest(tokens, candidates)

                    ######################################################################### Equalization
                    recommendationsLSA = equalization(recommendationsLSA)
                    recommendationsNaive = equalization(recommendationsNaive)

                    ######################################################################### Teta + correct
                    recommendations = []
                    for recom in range(0, len(recommendationsLSA)):
                        recommendation = alpha * recommendationsNaive[recom] + (1.0 - alpha) * recommendationsLSA[recom]
                        recommendations.append(recommendation)

                    idx = recommendations.index(max(recommendations))
                    ans = choices[idx]
                    prediction.append(ans)

            acc = 0
            for i in range(0, len(answers)):
                if answers[i] == prediction[i]:
                    acc += 1
            evaluation = float(acc) / float(len(answers))

            print('Evaluation: ' + str(evaluation))

            with open(config['path_test'] + '/evaluation.txt', 'a') as archive:
                l = str(test_count) + '@' + str(evaluation)
                l = l + "\n"
                archive.write(l)

                test_count += 1

            print("Done in {:3f}s".format(time.time() - start))

        data = {}
        data['evaluation'] = str(evaluation)
        data['alpha'] = str(alpha)
        data['lambdas'] = naive.weightsPrevious

        res = json.dumps(data)

        return app.response_class(res, content_type='application/json')

    else:
        return 'no json received'

@app.route('/testfull', methods=['POST'])
def jsonTestFullPost():
    if request.json:
        eta = config['eta']
        etaLambda = config['etaLambda']
        times = config['times']
        param_update = config['param_update']

        mydata = request.json

        if mydata.get('alpha'):
            alpha = float(mydata.get('alpha'))
        if mydata.get('eta'):
            eta = float(mydata.get('eta'))
        if mydata.get('etaLambda'):
            etaLambda = float(mydata.get('etaLambda'))
        if mydata.get('times'):
            times = int(mydata.get('times'))
        if mydata.get('param_update'):
            param_update = int(mydata.get('param_update'))
        if mydata.get('lambdas_previous'):
            naive.weightsPrevious = mydata.get('lambdas_previous')
        if mydata.get('lambdas_post'):
            naive.weightsPost = mydata.get('lambdas_post')
        if mydata.get('index_test'):
            index_test = int(mydata.get('index_test'))
        if mydata.get('number_graphs'):
            number_graphs = int(mydata.get('number_graphs'))

        answersDict = csv.DictReader(open(config['path_test'] + '/test_answer.csv'))
        answers = []
        for row in answersDict:
            answers.append(row['answer'])

        alphas = []
        errors = []
        keys = ['a)', 'b)', 'c)', 'd)', 'e)']
        choices = ['a', 'b', 'c', 'd', 'e']

        if os.path.exists(config['path_test'] + '/evaluation.txt'):
            os.remove(config['path_test'] + '/evaluation.txt')
        if os.path.exists(config['path_test'] + '/alpha.txt'):
            os.remove(config['path_test'] + '/alpha.txt')
        if os.path.exists(config['path_test'] + '/error.txt'):
            os.remove(config['path_test'] + '/error.txt')

        test_count = 1

        if not mydata.get('lambdas_previous'):
            naive.weightsPrevious = []
            weightsProd = 1.0
            for ng in range(0, number_graphs):
                naive.weightsPrevious.append(random.uniform(0.0,1.0))
                if ng != 0:
                    weightsProd *= naive.weightsPrevious[ng]
            naive.weightsPrevious[0] = 1.0 / weightsProd
        if not mydata.get('lambdas_post'):
            naive.weightsPost = []
            weightsProd = 1.0
            for ng in range(0, number_graphs):
                naive.weightsPost.append(random.uniform(0.0,1.0))
                if ng != 0:
                    weightsProd *= naive.weightsPost[ng]
            naive.weightsPost[0] = 1.0 / weightsProd

        for file_weights in glob.glob(config['path_test'] + '/weightsP*.txt'):
            os.remove(file_weights)

        questions = []
        # with open(lsa.path_test_file, 'r', encoding='utf-8') as f:
        #     reader = csv.DictReader(f)
        #     for i, row in enumerate(reader):
        #         questions.append(row)
        #
        # random.shuffle(questions)
        # questions = np.array_split(questions, 5)
        #
        # with open('tests/questions.pkl', 'wb') as fp:
        #     pickle.dump(questions, fp)

        with open('tests/questions.pkl', 'rb') as fp:
            questions = pickle.load(fp)

        questions_testing = questions[index_test]
        del questions[index_test]
        questions_training = np.concatenate(questions)

        alpha = random.uniform(0.0,1.0)

        for t in range(0, times):
            print('Time: ' + str(t+1))
            start = time.time()

            prediction = []

            error = 0.0

            if param_update:
                print("Optimizing")
                for row in questions_training:
                    question = row['question']
                    ulPos1 = question.index('_')
                    ulPos2 = question.rfind('_')

                    questionPrevious = question[:ulPos1]
                    questionPrevious = clean_punct(questionPrevious).split()

                    questionPost = question[ulPos2:]
                    questionPost = clean_punct(questionPost).split()

                    questionPrevious = questionPrevious[-number_graphs:]
                    questionPost = questionPost[:number_graphs]

                    # tokens = clean_punct(question).split()
                    tokens = questionPrevious + questionPost

                    candidates = []
                    candidates.append(row['a)'])
                    candidates.append(row['b)'])
                    candidates.append(row['c)'])
                    candidates.append(row['d)'])
                    candidates.append(row['e)'])

                    ########################################################################## Naive

                    questionPreviousIndex = conv_str_index(questionPrevious)
                    questionPostIndex = conv_str_index(questionPost)

                    if len(questionPreviousIndex) < number_graphs:
                        vec = []
                        for index in range(0, number_graphs - len(questionPreviousIndex)):
                            vec.append(fileManager.word_to_index.get('@'))
                        for wordIndex in questionPreviousIndex:
                            vec.append(wordIndex)
                        questionPreviousIndex = vec

                    if len(questionPostIndex) < number_graphs:
                        for index in range(0, number_graphs - len(questionPostIndex)):
                            questionPostIndex.append(fileManager.word_to_index.get('@'))

                    optionsIndex = []
                    for word in candidates:
                        if word in fileManager.word_to_index and word != '':
                            optionsIndex.append(fileManager.word_to_index.get(word))
                        else:
                            optionsIndex.append(fileManager.word_to_index.get('@'))

                    recommendationsNaive = naive.doHypothesisTestFull(questionPreviousIndex, questionPostIndex, optionsIndex)

                    ########################################################################## LSA

                    recommendationsLSA = lsa.doHypothesisTest(tokens, candidates)

                    ######################################################################### Equalization

                    recommendationsLSA = equalization(recommendationsLSA)
                    recommendationsNaive = equalization(recommendationsNaive)

                    ######################################################################### Teta + correct

                    recommendations = []
                    for recom in range(0, len(recommendationsLSA)):
                        recommendation = alpha * recommendationsNaive[recom] + (1.0 - alpha) * recommendationsLSA[recom]
                        recommendations.append(recommendation)

                    idx = recommendations.index(max(recommendations))
                    ans = choices[idx]
                    prediction.append(ans)

                    correctIndex = choices.index(answers[int(row['id'])-1])
                    teta = recommendations[correctIndex]
                    tetaBayes = recommendationsNaive[correctIndex]

                    ######################################################################### Lambda

                    if etaLambda != 0.0:
                        naive.updateLambdaPrevious(questionPreviousIndex, optionsIndex, alpha, teta, tetaBayes, etaLambda, correctIndex)
                        naive.updateLambdaPost(questionPostIndex, optionsIndex, alpha, teta, tetaBayes, etaLambda, correctIndex)

                    ######################################################################### Alpha
                    alpha = alpha + (1.0 - teta) * (recommendationsNaive[correctIndex] - recommendationsLSA[correctIndex]) * eta
                    if alpha < 0.0:
                        alpha = 0.0
                    elif alpha > 1.0:
                        alpha = 1.0

                    error += 0.5*((1.0 - teta)**2.0)

                    alphas.append(alpha)

                    w_index = 0
                    for weight in naive.weightsPrevious:
                        with open(config['path_test'] + '/weightsPrevious' + str(w_index) + '.txt', 'a') as archive:
                            l = str(weight)
                            l = l + '\n';
                            archive.write(l);
                            w_index += 1
                    w_index = 0
                    for weight in naive.weightsPost:
                        with open(config['path_test'] + '/weightsPost' + str(w_index) + '.txt', 'a') as archive:
                            l = str(weight)
                            l = l + '\n';
                            archive.write(l);
                            w_index += 1

                    # print ('Alpha: ' + str(alpha))
                    # print ('Lambdas: ' + str(naive.weightsPrevious))

            with open(config['path_test'] + '/alpha.txt', 'w') as archive:
                for a in alphas:
                    l = str(a);
                    l = l + "\n";
                    archive.write(l);

            errors.append(error)

            prediction = []

            print("Testing")
            for row in questions_testing:
                question = row['question']
                ulPos1 = question.index('_')
                ulPos2 = question.rfind('_')

                questionPrevious = question[:ulPos1]
                questionPrevious = clean_punct(questionPrevious).split()

                questionPost = question[ulPos2:]
                questionPost = clean_punct(questionPost).split()

                questionPrevious = questionPrevious[-number_graphs:]
                questionPost = questionPost[:number_graphs]

                # tokens = clean_punct(question).split()
                tokens = questionPrevious + questionPost

                candidates = []
                candidates.append(row['a)'])
                candidates.append(row['b)'])
                candidates.append(row['c)'])
                candidates.append(row['d)'])
                candidates.append(row['e)'])

                ########################################################################## Naive

                questionPreviousIndex = conv_str_index(questionPrevious)
                questionPostIndex = conv_str_index(questionPost)

                if len(questionPreviousIndex) < number_graphs:
                    vec = []
                    for index in range(0, number_graphs - len(questionPreviousIndex)):
                        vec.append(fileManager.word_to_index.get('@'))
                    for wordIndex in questionPreviousIndex:
                        vec.append(wordIndex)
                    questionPreviousIndex = vec

                if len(questionPostIndex) < number_graphs:
                    for index in range(0, number_graphs - len(questionPostIndex)):
                        questionPostIndex.append(fileManager.word_to_index.get('@'))

                optionsIndex = []
                for word in candidates:
                    if word in fileManager.word_to_index and word != '':
                        optionsIndex.append(fileManager.word_to_index.get(word))
                    else:
                        optionsIndex.append(fileManager.word_to_index.get('@'))

                recommendationsNaive = naive.doHypothesisTestFull(questionPreviousIndex, questionPostIndex, optionsIndex)

                ########################################################################## LSA

                recommendationsLSA = lsa.doHypothesisTest(tokens, candidates)

                ######################################################################### Equalization

                recommendationsLSA = equalization(recommendationsLSA)
                recommendationsNaive = equalization(recommendationsNaive)

                ######################################################################### Teta + correct

                recommendations = []
                for recom in range(0, len(recommendationsLSA)):
                    recommendation = alpha * recommendationsNaive[recom] + (1.0 - alpha) * recommendationsLSA[recom]
                    recommendations.append(recommendation)

                idx = recommendations.index(max(recommendations))
                ans = choices[idx]
                prediction.append({'id':int(row['id'])-1, 'answer':ans})

            acc = 0
            for p in prediction:
                if answers[p.get('id')] == p.get('answer'):
                    acc += 1
            evaluation = float(acc) / float(len(questions_testing))

            print('Evaluation: ' + str(evaluation))

            with open(config['path_test'] + '/evaluation.txt', 'a') as archive:
                l = str(evaluation);
                l = l + "\n";
                archive.write(l);

                test_count += 1

            with open(config['path_test'] + '/error.txt', 'w') as archive:
                for e in errors:
                    l = str(e);
                    l = l + "\n";
                    archive.write(l);

            print("Done in {:3f}s".format(time.time() - start))

        data = {}
        data['error'] = str(errors[-1])
        data['evaluation'] = str(evaluation)
        data['alpha'] = str(alpha)
        data['lambdas_previous'] = naive.weightsPrevious
        data['lambdas_post'] = naive.weightsPost

        res = json.dumps(data)

        return app.response_class(res, content_type='application/json')

    else:
        return 'no json received'

def equalization(vector):
    vector = np.array(vector)
    pos = np.argsort(vector) + 1
    s = sum(pos)

    count = 1
    for p in pos:
        new_value = count / s
        vector[p - 1] = new_value
        count += 1

    return vector


def clean_str(text):
    text = re.sub(r"\[([^\]]+)\]", " ", text)
    text = re.sub(r"\(([^\)]+)\)", " ", text)
    text = re.sub(r"[^A-Za-z,!?.;]", " ", text)
    text = re.sub(r",", " , ", text)
    text = re.sub(r"!", " ! ", text)
    text = re.sub(r"\?", " ? ", text)
    text = re.sub(r";", " ; ", text)
    text = re.sub(r"\s{2,}", " ", text)

    translator = str.maketrans('', '', string.punctuation)
    text = text.translate(translator)

    return text.strip().lower()


def clean_punct(text):
    text = re.sub("[^A-z0-9' -]", '', text).replace('`', '')

    translator = str.maketrans('', '', string.punctuation)
    text = text.translate(translator)

    return text.strip().lower()


def conv_str_index(text):
    stringCleared = []

    for word in text:
        if word in fileManager.word_to_index and word != '':
            stringCleared.append(fileManager.word_to_index.get(word))
        else:
            stringCleared.append(fileManager.word_to_index.get('@'))

    return stringCleared

if __name__ == '__main__':
    config = {}
    with open('config.json', 'r') as fp:
        config = json.load(fp)
        number_graphs = config['number_graphs']
        alpha = config['alpha']
        r = config['r']

    fileManager = FileManager()

    if not config['open_file_naive']:
        if not config['open_file_naive']:
            fileManager.fileOpenNSF(config['path_database1'])
            fileManager.fileOpenMauro(config['path_database2'])
            # fileManager.fileOpenHolmes(config['path_database3'])

        if not config['open_file_naive']:
            fileManager.setIndex()
            fileManager.setGraph(number_graphs)

    del fileManager.database

    fileManager.archive(number_graphs)

    word_total = fileManager.word_total

    word_count = len(fileManager.word_to_index)
    print('Words: ' + str(word_count))
    print('Total of words: ' + str(word_total))

    graph = Graph(word_total, fileManager.sum_col_db, fileManager.sum_row_db, fileManager.graph_db)

    naive = NaiveBayes(fileManager.aPriori, word_total, number_graphs, fileManager.word_to_index, fileManager.index_to_word, graph)
    naive.lambdas_previous = config['lambdas_previous']
    naive.lambdas_post = config['lambdas_post']

    data_lsa = []
    data_lsa.append(config['path_database3'])

    lsa = LSA(fileManager.word_to_index, fileManager.index_to_word, data_lsa, config['path_test_file'], config['path_output'], config['path_training'], graph)
    # lsa.testLSA()

    app.run(host='0.0.0.0', port=6000, threaded=True)
