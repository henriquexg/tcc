# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import os
from os.path import expanduser
from scipy import signal

if __name__ == '__main__':
    home = expanduser("~")
    path1 = "/home/henrique/tcc/tests/error1.txt"
    path2 = "/home/henrique/tcc/tests/error2.txt"
    path3 = "/home/henrique/tcc/tests/error3.txt"
    path4 = "/home/henrique/tcc/tests/error4.txt"
    path5 = "/home/henrique/tcc/tests/error5.txt"

    errors1 = []

    if os.path.exists(path1):
        with open(path1, "r") as archive:
            for line in archive:
                errors1.append(float(line))

    errors2 = []

    if os.path.exists(path2):
        with open(path2, "r") as archive:
            for line in archive:
                errors2.append(float(line))

    errors3 = []

    if os.path.exists(path3):
        with open(path3, "r") as archive:
            for line in archive:
                errors3.append(float(line))

    errors4 = []

    if os.path.exists(path4):
        with open(path4, "r") as archive:
            for line in archive:
                errors4.append(float(line))

    errors5 = []

    if os.path.exists(path5):
        with open(path5, "r") as archive:
            for line in archive:
                errors5.append(float(line))

    plt.xlabel(u"Epochs")
    plt.ylabel(u"Error")

    # b, a = signal.butter(1, 0.2, analog=False)
    #
    # errors1 = signal.filtfilt(b, a, errors1)
    # errors2 = signal.filtfilt(b, a, errors2)
    # errors3 = signal.filtfilt(b, a, errors3)
    # errors4 = signal.filtfilt(b, a, errors4)
    # errors5 = signal.filtfilt(b, a, errors5)

    p1 = plt.plot(range(1,len(errors1)+1), errors1, 'b', marker='>', markevery=10, label='Config. 1')
    p2 = plt.plot(range(1,len(errors1)+1), errors2, 'r', marker='<', markevery=10, label='Config. 2')
    p3 = plt.plot(range(1,len(errors1)+1), errors3, 'g', marker='8', markevery=10, label='Config. 3')
    p4 = plt.plot(range(1,len(errors1)+1), errors4, 'yellow', marker='x', markevery=10, markersize=12, label='Config. 4')
    p5 = plt.plot(range(1,len(errors1)+1), errors5, 'black', marker='*', markevery=10, markersize=12, label='Config. 5')

    plt.legend(numpoints=1)
    plt.show()
